#!/usr/bin//env sh

copyright-header --license GPL3 --copyright-holder 'Kamensky Timofey' --copyright-software 'TizenEnv' --copyright-software-description 'An utility for setting up a Tizen development environment.' --copyright-year '2016' --syntax ~/src/projects/copyright-header-syntax.yml --add-path src --output-dir ./
