{-|
TizenEnv - An utility for setting up a Tizen development environment.
Copyright (C) 2016 Kamensky Timofey

This file is part of TizenEnv.

TizenEnv is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TizenEnv is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TizenEnv.  If not, see <http://www.gnu.org/licenses/>.
-}
{-# LANGUAGE OverloadedStrings, RecordWildCards #-}

module TizenEnv.EnvVars ( createBuildEnv
                        ) where

import Prelude hiding (FilePath, pi)
import Data.Text (Text)
import qualified Data.Text as Text
import Data.Map (Map)
import qualified Data.Map as Map
import qualified Data.List as List

import TizenEnv.SBI
import TizenEnv.PackageInfo
import TizenEnv.Utils
  
createBuildEnv :: SBIConfig -> DevPackage -> Map Text Text
createBuildEnv sbi dp =
  let cc = sbiExpToolPath sbi "c_compiler"
      cxx = sbiExpToolPath sbi "c++_compiler"
      link = sbiExpToolPath sbi "llvm_linker"
      gdb = sbiExpToolPath sbi "debugger"
      as = sbiExpToolPath sbi "assembler"
      gcc = sbiExpToolPath sbi "gcc"
      edje_cc = sbiExpToolPath sbi "edje_cc"
      make = sbiExpToolPath sbi "make"
      tpk = sbiExpToolPath sbi "tpk_packager"
      native_signer = sbiExpToolPath sbi "native_signer"
      binutils = sbiExpToolPath sbi "binutils"
                 
      tcLinkerFlags = sbiExpToolchainProp sbi "LINKER_MISCELLANEOUS_OPTION"
      tcCFlags = sbiExpToolchainProp sbi "COMPILER_MISCELLANEOUS_OPTION"
                   
      rsLinkerFlags = sbiExpRootstrapProp sbi "LINKER_MISCELLANEOUS_OPTION"
      rsCFlags = sbiExpRootstrapProp sbi "COMPILER_MISCELLANEOUS_OPTION"
      rsLibraries = sbiExpRootstrapProp sbi "RS_LIBRARIES"

      sysroot = flip Text.append "/" . expandVars sbi . rsPath . sbiRootstrap $ sbi

      includePaths = fmap (Text.append sysroot) (devpIncludePaths dp)
      libraryPaths = fmap (Text.append sysroot) (devpLibraryPaths dp)

      includeFlags = fmap (Text.append "-I") includePaths
      libraryFlags = fmap (Text.append "-L") libraryPaths

      sysrootFlag = Text.append "--sysroot=" sysroot
                     
      cflags = Text.concat . List.intersperse " "
               $ consMaybe rsCFlags . consMaybe tcCFlags
               $ sysrootFlag : includeFlags
      cxxflags = cflags
                 
      ldflags = Text.concat . List.intersperse " "
                $ consMaybe rsLinkerFlags . consMaybe tcLinkerFlags
                . consMaybe rsLibraries $ sysrootFlag : libraryFlags

      cpath = Text.concat . List.intersperse ":" $ includePaths
      library_path = Text.concat . List.intersperse ":" $ libraryPaths
      ld_library_path = library_path
                
  in Map.fromList $
     consMaybeVar "CC" cc .
     consMaybeVar "CXX" cxx .
     consMaybeVar "LINK" link .
     consMaybeVar "GDB" gdb .
     consMaybeVar "AS" as .
     consMaybeVar "GCC" gcc .
     consMaybeVar "EDJE_CC" edje_cc .
     consMaybeVar "MAKE" make .
     consMaybeVar "TPK" tpk .
     consMaybeVar "NATIVE_SIGNER" native_signer .
     consMaybeVar "BINUTILS" binutils .
     
     consVar "SYSROOT" sysroot .
     
     consVar "CFLAGS" cflags .
     consVar "CXXFLAGS" cxxflags .
     consVar "LDFLAGS" ldflags .
     
     consVar "CPATH" cpath .
     consVar "LIBRARY_PATH" library_path .
     consVar "LD_LIBRARY_PATH" ld_library_path .

     id $ []

-- clang -MMD -MT src/basicuiapplication.o -MF src/basicuiapplication.o.d  -I"/mnt/extra0/tima/workspace/BasicUIApplication/inc" -O0 -g3 -Wall -c -fmessage-length=0 -target i386-tizen-linux-gnueabi -gcc-toolchain /mnt/extra0/tima/tizen-sdk/tools/smart-build-interface/../i386-linux-gnueabi-gcc-4.9/ -ccc-gcc-name i386-linux-gnueabi-g++ -march=i386 -Wno-gnu  -fPIE --sysroot="/mnt/extra0/tima/tizen-sdk/tools/smart-build-interface/../../platforms/tizen-2.4/mobile/rootstraps/mobile-2.4-emulator.core" -I"/mnt/extra0/tima/tizen-sdk/tools/smart-build-interface/../../platforms/tizen-2.4/mobile/rootstraps/mobile-2.4-emulator.core/usr/include/libxml2" -I"/mnt/extra0/tima/tizen-sdk/tools/smart-build-interface/../../platforms/tizen-2.4/mobile/rootstraps/mobile-2.4-emulator.core/usr/include" -I"/mnt/extra0/tima/tizen-sdk/tools/smart-build-interface/../../platforms/tizen-2.4/mobile/rootstraps/mobile-2.4-emulator.core/usr/include/appcore-agent" -I"/mnt/extra0/tima/tizen-sdk/tools/smart-build-interface/../../platforms/tizen-2.4/mobile/rootstraps/mobile-2.4-emulator.core/usr/include/appfw" -I"/mnt/extra0/tima/tizen-sdk/tools/smart-build-interface/../../platforms/tizen-2.4/mobile/rootstraps/mobile-2.4-emulator.core/usr/include/attach-panel" -I"/mnt/extra0/tima/tizen-sdk/tools/smart-build-interface/../../platforms/tizen-2.4/mobile/rootstraps/mobile-2.4-emulator.core/usr/include/badge" -I"/mnt/extra0/tima/tizen-sdk/tools/smart-build-interface/../../platforms/tizen-2.4/mobile/rootstraps/mobile-2.4-emulator.core/usr/include/base" -I"/mnt/extra0/tima/tizen-sdk/tools/smart-build-interface/../../platforms/tizen-2.4/mobile/rootstraps/mobile-2.4-emulator.core/usr/include/cairo" -I"/mnt/extra0/tima/tizen-sdk/tools/smart-build-interface/../../platforms/tizen-2.4/mobile/rootstraps/mobile-2.4-emulator.core/usr/include/calendar-service2" -I"/mnt/extra0/tima/tizen-sdk/tools/smart-build-interface/../../platforms/tizen-2.4/mobile/rootstraps/mobile-2.4-emulator.core/usr/include/ckm" -I"/mnt/extra0/tima/tizen-sdk/tools/smart-build-interface/../../platforms/tizen-2.4/mobile/rootstraps/mobile-2.4-emulator.core/usr/include/contacts-svc" -I"/mnt/extra0/tima/tizen-sdk/tools/smart-build-interface/../../platforms/tizen-2.4/mobile/rootstraps/mobile-2.4-emulator.core/usr/include/content" -I"/mnt/extra0/tima/tizen-sdk/tools/smart-build-interface/../../platforms/tizen-2.4/mobile/rootstraps/mobile-2.4-emulator.core/usr/include/context-service" -I"/mnt/extra0/tima/tizen-sdk/tools/smart-build-interface/../../platforms/tizen-2.4/mobile/rootstraps/mobile-2.4-emulator.core/usr/include/dali" -I"/mnt/extra0/tima/tizen-sdk/tools/smart-build-interface/../../platforms/tizen-2.4/mobile/rootstraps/mobile-2.4-emulator.core/usr/include/dali-toolkit" -I"/mnt/extra0/tima/tizen-sdk/tools/smart-build-interface/../../platforms/tizen-2.4/mobile/rootstraps/mobile-2.4-emulator.core/usr/include/dbus-1.0" -I"/mnt/extra0/tima/tizen-sdk/tools/smart-build-interface/../../platforms/tizen-2.4/mobile/rootstraps/mobile-2.4-emulator.core/usr/include/device" -I"/mnt/extra0/tima/tizen-sdk/tools/smart-build-interface/../../platforms/tizen-2.4/mobile/rootstraps/mobile-2.4-emulator.core/usr/include/dlog" -I"/mnt/extra0/tima/tizen-sdk/tools/smart-build-interface/../../platforms/tizen-2.4/mobile/rootstraps/mobile-2.4-emulator.core/usr/include/ecore-1" -I"/mnt/extra0/tima/tizen-sdk/tools/smart-build-interface/../../platforms/tizen-2.4/mobile/rootstraps/mobile-2.4-emulator.core/usr/include/ecore-buffer-1" -I"/mnt/extra0/tima/tizen-sdk/tools/smart-build-interface/../../platforms/tizen-2.4/mobile/rootstraps/mobile-2.4-emulator.core/usr/include/ecore-con-1" -I"/mnt/extra0/tima/tizen-sdk/tools/smart-build-interface/../../platforms/tizen-2.4/mobile/rootstraps/mobile-2.4-emulator.core/usr/include/ecore-evas-1" -I"/mnt/extra0/tima/tizen-sdk/tools/smart-build-interface/../../platforms/tizen-2.4/mobile/rootstraps/mobile-2.4-emulator.core/usr/include/ecore-file-1" -I"/mnt/extra0/tima/tizen-sdk/tools/smart-build-interface/../../platforms/tizen-2.4/mobile/rootstraps/mobile-2.4-emulator.core/usr/include/ecore-imf-1" -I"/mnt/extra0/tima/tizen-sdk/tools/smart-build-interface/../../platforms/tizen-2.4/mobile/rootstraps/mobile-2.4-emulator.core/usr/include/ecore-imf-evas-1" -I"/mnt/extra0/tima/tizen-sdk/tools/smart-build-interface/../../platforms/tizen-2.4/mobile/rootstraps/mobile-2.4-emulator.core/usr/include/ecore-input-1" -I"/mnt/extra0/tima/tizen-sdk/tools/smart-build-interface/../../platforms/tizen-2.4/mobile/rootstraps/mobile-2.4-emulator.core/usr/include/ecore-input-evas-1" -I"/mnt/extra0/tima/tizen-sdk/tools/smart-build-interface/../../platforms/tizen-2.4/mobile/rootstraps/mobile-2.4-emulator.core/usr/include/ecore-ipc-1" -I"/mnt/extra0/tima/tizen-sdk/tools/smart-build-interface/../../platforms/tizen-2.4/mobile/rootstraps/mobile-2.4-emulator.core/usr/include/ecore-x-1" -I"/mnt/extra0/tima/tizen-sdk/tools/smart-build-interface/../../platforms/tizen-2.4/mobile/rootstraps/mobile-2.4-emulator.core/usr/include/e_dbus-1" -I"/mnt/extra0/tima/tizen-sdk/tools/smart-build-interface/../../platforms/tizen-2.4/mobile/rootstraps/mobile-2.4-emulator.core/usr/include/edje-1" -I"/mnt/extra0/tima/tizen-sdk/tools/smart-build-interface/../../platforms/tizen-2.4/mobile/rootstraps/mobile-2.4-emulator.core/usr/include/eet-1" -I"/mnt/extra0/tima/tizen-sdk/tools/smart-build-interface/../../platforms/tizen-2.4/mobile/rootstraps/mobile-2.4-emulator.core/usr/include/efl-1" -I"/mnt/extra0/tima/tizen-sdk/tools/smart-build-interface/../../platforms/tizen-2.4/mobile/rootstraps/mobile-2.4-emulator.core/usr/include/efl-extension" -I"/mnt/extra0/tima/tizen-sdk/tools/smart-build-interface/../../platforms/tizen-2.4/mobile/rootstraps/mobile-2.4-emulator.core/usr/include/efreet-1" -I"/mnt/extra0/tima/tizen-sdk/tools/smart-build-interface/../../platforms/tizen-2.4/mobile/rootstraps/mobile-2.4-emulator.core/usr/include/eina-1" -I"/mnt/extra0/tima/tizen-sdk/tools/smart-build-interface/../../platforms/tizen-2.4/mobile/rootstraps/mobile-2.4-emulator.core/usr/include/eina-1/eina" -I"/mnt/extra0/tima/tizen-sdk/tools/smart-build-interface/../../platforms/tizen-2.4/mobile/rootstraps/mobile-2.4-emulator.core/usr/include/eio-1" -I"/mnt/extra0/tima/tizen-sdk/tools/smart-build-interface/../../platforms/tizen-2.4/mobile/rootstraps/mobile-2.4-emulator.core/usr/include/eldbus-1" -I"/mnt/extra0/tima/tizen-sdk/tools/smart-build-interface/../../platforms/tizen-2.4/mobile/rootstraps/mobile-2.4-emulator.core/usr/include/elementary-1" -I"/mnt/extra0/tima/tizen-sdk/tools/smart-build-interface/../../platforms/tizen-2.4/mobile/rootstraps/mobile-2.4-emulator.core/usr/include/embryo-1" -I"/mnt/extra0/tima/tizen-sdk/tools/smart-build-interface/../../platforms/tizen-2.4/mobile/rootstraps/mobile-2.4-emulator.core/usr/include/eo-1" -I"/mnt/extra0/tima/tizen-sdk/tools/smart-build-interface/../../platforms/tizen-2.4/mobile/rootstraps/mobile-2.4-emulator.core/usr/include/eom" -I"/mnt/extra0/tima/tizen-sdk/tools/smart-build-interface/../../platforms/tizen-2.4/mobile/rootstraps/mobile-2.4-emulator.core/usr/include/ethumb-1" -I"/mnt/extra0/tima/tizen-sdk/tools/smart-build-interface/../../platforms/tizen-2.4/mobile/rootstraps/mobile-2.4-emulator.core/usr/include/ethumb-client-1" -I"/mnt/extra0/tima/tizen-sdk/tools/smart-build-interface/../../platforms/tizen-2.4/mobile/rootstraps/mobile-2.4-emulator.core/usr/include/evas-1" -I"/mnt/extra0/tima/tizen-sdk/tools/smart-build-interface/../../platforms/tizen-2.4/mobile/rootstraps/mobile-2.4-emulator.core/usr/include/ewebkit2-0" -I"/mnt/extra0/tima/tizen-sdk/tools/smart-build-interface/../../platforms/tizen-2.4/mobile/rootstraps/mobile-2.4-emulator.core/usr/include/feedback" -I"/mnt/extra0/tima/tizen-sdk/tools/smart-build-interface/../../platforms/tizen-2.4/mobile/rootstraps/mobile-2.4-emulator.core/usr/include/fontconfig" -I"/mnt/extra0/tima/tizen-sdk/tools/smart-build-interface/../../platforms/tizen-2.4/mobile/rootstraps/mobile-2.4-emulator.core/usr/include/freetype2" -I"/mnt/extra0/tima/tizen-sdk/tools/smart-build-interface/../../platforms/tizen-2.4/mobile/rootstraps/mobile-2.4-emulator.core/usr/include/geofence" -I"/mnt/extra0/tima/tizen-sdk/tools/smart-build-interface/../../platforms/tizen-2.4/mobile/rootstraps/mobile-2.4-emulator.core/usr/include/gio-unix-2.0" -I"/mnt/extra0/tima/tizen-sdk/tools/smart-build-interface/../../platforms/tizen-2.4/mobile/rootstraps/mobile-2.4-emulator.core/usr/include/glib-2.0" -I"/mnt/extra0/tima/tizen-sdk/tools/smart-build-interface/../../platforms/tizen-2.4/mobile/rootstraps/mobile-2.4-emulator.core/usr/include/harfbuzz" -I"/mnt/extra0/tima/tizen-sdk/tools/smart-build-interface/../../platforms/tizen-2.4/mobile/rootstraps/mobile-2.4-emulator.core/usr/include/json-glib-1.0" -I"/mnt/extra0/tima/tizen-sdk/tools/smart-build-interface/../../platforms/tizen-2.4/mobile/rootstraps/mobile-2.4-emulator.core/usr/include/location" -I"/mnt/extra0/tima/tizen-sdk/tools/smart-build-interface/../../platforms/tizen-2.4/mobile/rootstraps/mobile-2.4-emulator.core/usr/include/maps" -I"/mnt/extra0/tima/tizen-sdk/tools/smart-build-interface/../../platforms/tizen-2.4/mobile/rootstraps/mobile-2.4-emulator.core/usr/include/media" -I"/mnt/extra0/tima/tizen-sdk/tools/smart-build-interface/../../platforms/tizen-2.4/mobile/rootstraps/mobile-2.4-emulator.core/usr/include/media-content" -I"/mnt/extra0/tima/tizen-sdk/tools/smart-build-interface/../../platforms/tizen-2.4/mobile/rootstraps/mobile-2.4-emulator.core/usr/include/messaging" -I"/mnt/extra0/tima/tizen-sdk/tools/smart-build-interface/../../platforms/tizen-2.4/mobile/rootstraps/mobile-2.4-emulator.core/usr/include/metadata-editor" -I"/mnt/extra0/tima/tizen-sdk/tools/smart-build-interface/../../platforms/tizen-2.4/mobile/rootstraps/mobile-2.4-emulator.core/usr/include/minicontrol" -I"/mnt/extra0/tima/tizen-sdk/tools/smart-build-interface/../../platforms/tizen-2.4/mobile/rootstraps/mobile-2.4-emulator.core/usr/include/minizip" -I"/mnt/extra0/tima/tizen-sdk/tools/smart-build-interface/../../platforms/tizen-2.4/mobile/rootstraps/mobile-2.4-emulator.core/usr/include/network" -I"/mnt/extra0/tima/tizen-sdk/tools/smart-build-interface/../../platforms/tizen-2.4/mobile/rootstraps/mobile-2.4-emulator.core/usr/include/notification" -I"/mnt/extra0/tima/tizen-sdk/tools/smart-build-interface/../../platforms/tizen-2.4/mobile/rootstraps/mobile-2.4-emulator.core/usr/include/phonenumber-utils" -I"/mnt/extra0/tima/tizen-sdk/tools/smart-build-interface/../../platforms/tizen-2.4/mobile/rootstraps/mobile-2.4-emulator.core/usr/include/sensor" -I"/mnt/extra0/tima/tizen-sdk/tools/smart-build-interface/../../platforms/tizen-2.4/mobile/rootstraps/mobile-2.4-emulator.core/usr/include/shortcut" -I"/mnt/extra0/tima/tizen-sdk/tools/smart-build-interface/../../platforms/tizen-2.4/mobile/rootstraps/mobile-2.4-emulator.core/usr/include/storage" -I"/mnt/extra0/tima/tizen-sdk/tools/smart-build-interface/../../platforms/tizen-2.4/mobile/rootstraps/mobile-2.4-emulator.core/usr/include/system" -I"/mnt/extra0/tima/tizen-sdk/tools/smart-build-interface/../../platforms/tizen-2.4/mobile/rootstraps/mobile-2.4-emulator.core/usr/include/telephony" -I"/mnt/extra0/tima/tizen-sdk/tools/smart-build-interface/../../platforms/tizen-2.4/mobile/rootstraps/mobile-2.4-emulator.core/usr/include/ui" -I"/mnt/extra0/tima/tizen-sdk/tools/smart-build-interface/../../platforms/tizen-2.4/mobile/rootstraps/mobile-2.4-emulator.core/usr/include/web" -I"/mnt/extra0/tima/tizen-sdk/tools/smart-build-interface/../../platforms/tizen-2.4/mobile/rootstraps/mobile-2.4-emulator.core/usr/include/widget_service" -I"/mnt/extra0/tima/tizen-sdk/tools/smart-build-interface/../../platforms/tizen-2.4/mobile/rootstraps/mobile-2.4-emulator.core/usr/include/widget_viewer_evas" -I"/mnt/extra0/tima/tizen-sdk/tools/smart-build-interface/../../platforms/tizen-2.4/mobile/rootstraps/mobile-2.4-emulator.core/usr/include/wifi-direct" -I"/mnt/extra0/tima/tizen-sdk/tools/smart-build-interface/../../platforms/tizen-2.4/mobile/rootstraps/mobile-2.4-emulator.core/usr/lib/dbus-1.0/include" -I"/mnt/extra0/tima/tizen-sdk/tools/smart-build-interface/../../platforms/tizen-2.4/mobile/rootstraps/mobile-2.4-emulator.core/usr/lib/glib-2.0/include" -Werror-implicit-function-declaration -c ../src/basicuiapplication.c -o src/basicuiapplication.o)

-- clang++ -o basicuiapplication src/basicuiapplication.o   -L"/mnt/extra0/tima/workspace/BasicUIApplication/lib" -target i386-tizen-linux-gnueabi -gcc-toolchain /mnt/extra0/tima/tizen-sdk/tools/smart-build-interface/../i386-linux-gnueabi-gcc-4.9/ -ccc-gcc-name i386-linux-gnueabi-g++ -march=i386 -Xlinker --as-needed  -pie -lpthread -Xlinker -rpath="/home/developer/sdk_tools/lib" --sysroot="/mnt/extra0/tima/tizen-sdk/tools/smart-build-interface/../../platforms/tizen-2.4/mobile/rootstraps/mobile-2.4-emulator.core" -Xlinker --version-script="/mnt/extra0/tima/workspace/BasicUIApplication/.exportMap" -L"/mnt/extra0/tima/tizen-sdk/tools/smart-build-interface/../../platforms/tizen-2.4/mobile/rootstraps/mobile-2.4-emulator.core/usr/lib" -lBrokenLocale -laccounts-svc -lanl -lappcore-agent -lattach-panel -lbadge -lbase-utils-i18n -lbundle -lcairo -lcalendar-service2 -lcapi-appfw-alarm -lcapi-appfw-app-common -lcapi-appfw-app-control -lcapi-appfw-application -lcapi-appfw-app-manager -lcapi-appfw-event -lcapi-appfw-package-manager -lcapi-appfw-preference -lcapi-appfw-widget-application -lcapi-base-common -lcapi-content-media-content -lcapi-content-mime-type -lcapi-data-control -lcapi-geofence-manager -lcapi-location-manager -lcapi-maps-service -lcapi-media-audio-io -lcapi-media-camera -lcapi-media-codec -lcapi-media-controller -lcapi-media-image-util -lcapi-media-metadata-editor -lcapi-media-metadata-extractor -lcapi-media-player -lcapi-media-radio -lcapi-media-recorder -lcapi-media-screen-mirroring -lcapi-media-sound-manager -lcapi-media-thumbnail-util -lcapi-media-tone-player -lcapi-media-tool -lcapi-media-video-util -lcapi-media-vision -lcapi-media-wav-player -lcapi-message-port -lcapi-messaging-email -lcapi-messaging-messages -lcapi-network-bluetooth -lcapi-network-connection -lcapi-network-nfc -lcapi-network-smartcard -lcapi-network-wifi -lcapi-system-device -lcapi-system-info -lcapi-system-media-key -lcapi-system-runtime-info -lcapi-system-sensor -lcapi-system-system-settings -lcapi-telephony -lcapi-ui-efl-util -lcapi-ui-inputmethod-manager -lcapi-ui-inputmethod -lcapi-web-url-download -lcidn -lcontacts-service2 -lcontext -lcore-context-manager -lcore-sync-client -lcrypto -lcrypt -lc -lcurl -ldali-adaptor -ldali-core -ldali-toolkit -ldlog -ldl -lebluez -leconnman0_7x -lecore_buffer -lecore_con -lecore_evas -lecore_file -lecore_imf_evas -lecore_imf -lecore_input_evas -lecore_input -lecore_ipc -lecore -lecore_x -ledbus -ledje -leet -lefl-extension -lefreet_mime -lefreet -lefreet_trash -lehal -leina -leio -lelementary -lembryo -lenotify -leofono -leom -leo -lethumb_client -lethumb -leukit -levas -lewebkit2 -lexif -lfeedback -lfontconfig -lfreetype -lgio-2.0 -lglib-2.0 -lgmodule-2.0 -lgobject-2.0 -lgthread-2.0 -lharfbuzz-icu -lharfbuzz -ljson-glib-1.0 -lkey-manager-client -lminicontrol-provider -lminicontrol-viewer -lminizip -lm -lnotification -lnsl -lnss_compat -lnss_dns -lnss_files -lnss_hesiod -lnss_nisplus -lnss_nis -loauth2 -loauth -lopenal -lphonenumber-utils -lprivilege-info -lpthread -lpush -lresolv -lrt -lservice-adaptor-client -lshortcut -lsqlite3 -lssl -lstorage -lstt -ltbm -lthread_db -lttrace -ltts -lutil -lvc-elm -lvc -lwidget_service -lwidget_viewer_evas -lwifi-direct -lxml2 -lz -Xlinker -rpath="/opt/usr/apps/org.example.basicuiapplication/lib" -Werror-implicit-function-declaration
