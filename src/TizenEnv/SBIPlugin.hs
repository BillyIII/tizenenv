{-|
TizenEnv - An utility for setting up a Tizen development environment.
Copyright (C) 2016 Kamensky Timofey

This file is part of TizenEnv.

TizenEnv is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TizenEnv is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TizenEnv.  If not, see <http://www.gnu.org/licenses/>.
-}
{-# LANGUAGE OverloadedStrings #-}

module TizenEnv.SBIPlugin ( Plugin(..)
                          , Extension(..)
                          , Attributes
                          , Properties
                          , Tools
                          , Actions
                          , Rootstrap(..)
                          , Toolchain(..)
                          , Tool(..)
                          , Action(..)
                          , ActionItem(..)
                          , ExecInput(..)
                          , Target(..)
                          ) where

import Data.Text (Text)
import Data.Map (Map)
-- import Filesystem.Path.CurrentOS

data Plugin = Plugin { plugExtPoint :: Text
                     , plugExtension :: Extension
                     }
            deriving (Eq, Show, Read)

data Extension = ExtToolchain Toolchain
               | ExtRootstrap Rootstrap
               | ExtTarget Target
               deriving (Eq, Show, Read)

type Attributes = Map Text Text
type Properties = Map Text Text
type Tools = Map Text Tool
type Actions = Map Text Action
                        
data Rootstrap = Rootstrap { rsId :: Text
                           , rsName :: Text
                           , rsVersion :: Text
                           , rsArchitecture :: Text
                           , rsPath :: Text
                           , rsSupportedToolchainType :: Text
                           , rsProperties :: Properties
                           }
               deriving (Eq, Show, Read)

data Toolchain = Toolchain { tcId :: Text
                           , tcName :: Text
                           , tcArchitecture :: Text
                           , tcPath :: Text
                           , tcToolchainType :: Text
                           , tcProperties :: Properties
                           , tcTools :: Tools
                           , tcActions :: Actions
                           }
               deriving (Eq, Show, Read)

data Tool = Tool { toolName :: Text
                 , toolVersion :: Maybe Text
                 , toolPath :: Text
                 }
          deriving (Eq, Show, Read)

data Action = Action { actName :: Text
                     , actItems :: [ActionItem]
                     }
            deriving (Eq, Show, Read)

data ActionItem = Execute { execScript :: Text
                          , execInputs :: [ExecInput]
                          , execBody :: Text
                          }
                deriving (Eq, Show, Read)
                  
data ExecInput = ExecInput { inputType :: Text
                           , inputValue :: Text
                           }
               deriving (Eq, Show, Read)

data Target = Target { targId :: Text
                     , targName :: Text
                     , targRootstrapId :: Text
                     , targToolchainId :: Text
                     }
            deriving (Eq, Show, Read)
