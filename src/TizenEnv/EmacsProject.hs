{-|
TizenEnv - An utility for setting up a Tizen development environment.
Copyright (C) 2016 Kamensky Timofey

This file is part of TizenEnv.

TizenEnv is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TizenEnv is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TizenEnv.  If not, see <http://www.gnu.org/licenses/>.
-}
{-# LANGUAGE OverloadedStrings #-}

module TizenEnv.EmacsProject ( Label
                             , Project(..)
                             , Value(..)
                             , labelText
                             , mklabel
                             , parseProjectString
                             , makeProjectString
                             , readProject
                             , writeProject
                             , reconfigureProject
                             ) where

import Prelude hiding (FilePath, pi)
import Filesystem.Path.CurrentOS
import Data.Text (Text)
import qualified Data.Text as Text
import Data.Text.Encoding (encodeUtf8, decodeUtf8)
import Data.Map (Map)
import qualified Data.Map as Map
import qualified Language.Sexp as S
import qualified Data.ByteString.Lazy as BL
import qualified Data.List as List
import Data.String (IsString(..))

import TizenEnv.SBI
import TizenEnv.PackageInfo
import TizenEnv.Utils

newtype Label = Label { labelText :: Text
                      }
              deriving (Show, Read, Eq, Ord)

mklabel :: Text -> Label
mklabel = Label

instance IsString Label where
  fromString = Label . fromString

data Project = Project { projType :: Text
                       , projName :: Text
                       , projFields :: Map Label Value
                       }
             deriving (Show, Read)

data Value = StringValue Text
           | LabelValue Label
           | ListValue Bool [Value]
           | PairValue Value Value
           | MapValue Bool (Map Value Value)
           deriving (Show, Read, Eq, Ord)

readProject :: FilePath -> IO Project
readProject path = do
  str <- BL.readFile (encodeString path)
  proj <- parseProjectString str
  return proj
               
writeProject :: FilePath -> Project -> IO ()
writeProject path proj = do
  let str = makeProjectString proj
  BL.writeFile (encodeString path) str

parseProjectString :: (Monad m, Applicative m) => BL.ByteString -> m Project
parseProjectString str = do
  let [sexp] = S.parseExn str
  proj <- S.fromSexp sexp
  return proj

makeProjectString :: Project -> BL.ByteString
makeProjectString = S.printHum . S.toSexp
    
mkstr :: Text -> Value
mkstr t = StringValue $ Text.concat ["\"", t, "\""]

mkstrlist :: [Text] -> Value
mkstrlist = ListValue True . fmap mkstr
  
reconfigureProject :: SBIConfig -> DevPackage -> Project -> Project
-- reconfigureProject = error "Not implemented yet."
reconfigureProject sbi dp proj =
  let -- cxx = sbiExpToolPath sbi "c++_compiler"
            
      tcCFlags = sbiExpToolchainProp sbi "COMPILER_MISCELLANEOUS_OPTION"
      rsCFlags = sbiExpRootstrapProp sbi "COMPILER_MISCELLANEOUS_OPTION"
                 
      sysroot = flip Text.append "/" . expandVars sbi . rsPath . sbiRootstrap $ sbi

      includePaths = fmap (Text.append sysroot) (devpIncludePaths dp)

      sysrootFlag = Text.append "--sysroot=" sysroot
                     
      cxxflags = Text.concat . List.intersperse " "
                 $ consMaybe rsCFlags . consMaybe tcCFlags
                 $ sysrootFlag : []

      sysIncludesValue = mkstrlist includePaths
      extraFlagsValue = mkstrlist . List.filter (not . Text.null) . Text.splitOn " " $ cxxflags
                         
      fields = Map.insert ":system-include-path" sysIncludesValue .
               Map.insert ":extra-flags" extraFlagsValue $
               (projFields proj)

  in proj { projFields = fields }

instance S.Sexpable Project where
     toSexp = projectToSexp
     fromSexp = sexpToProject

text2bs :: Text -> BL.ByteString
text2bs = BL.fromStrict . encodeUtf8

text2atom :: Text -> S.Sexp
text2atom = S.Atom . text2bs

projectToSexp :: Project -> S.Sexp
projectToSexp p = S.List ((text2atom $ projType p) :
                          (text2atom $ projName p) :
                          (fieldsToSexp (projFields p))
                         )

fieldsToSexp :: Map Label Value -> [S.Sexp]
fieldsToSexp = List.concatMap field2sexps . Map.assocs
  where field2sexps (Label label, value) = (S.Atom $ text2bs label) : (valueToSexp value)

valueToSexp :: Value -> [S.Sexp]
valueToSexp (StringValue v) = [text2atom v]
valueToSexp (LabelValue (Label v)) = [text2atom v]
valueToSexp (ListValue q vs ) = if q
                                then [S.Atom "'", listToSexp vs]
                                else [listToSexp vs]
valueToSexp (PairValue k v) = [listToSexp [k, StringValue ".", v]]
valueToSexp (MapValue q m) = if q
                             then [S.Atom "'", mapToSexp m]
                             else [mapToSexp m]

listToSexp :: [Value] -> S.Sexp
listToSexp = S.List . List.concatMap valueToSexp

mapToSexp :: Map Value Value -> S.Sexp
mapToSexp = listToSexp . fmap (uncurry PairValue)  . Map.assocs
                
bs2text :: BL.ByteString -> Text
bs2text = decodeUtf8 . BL.toStrict
                
sexpToProject :: (Monad m, Applicative m) => S.Sexp -> m Project
sexpToProject (S.List ((S.Atom type_):(S.Atom name):fields)) = do
  fs <- parseFields fields
  return $ Project (bs2text type_) (bs2text name) (Map.fromList fs)
sexpToProject _ =
  fail "Invalid project structure."

parseFields :: (Monad m, Applicative m) => [S.Sexp] -> m [(Label, Value)]
parseFields ((S.Atom name):list) = do
  (val, rest) <- parseFieldValue list
  fields <- parseFields rest
  return $ (Label (bs2text name), val):fields
parseFields [] = return []
parseFields v = error $ "Unexpected value: " ++ (show v)
  
parseFieldValue :: (Monad m, Applicative m) => [S.Sexp] -> m (Value, [S.Sexp])
-- '(...)
parseFieldValue ((S.Atom "'"):(S.List list):rest) =
  parseFieldList True list rest
-- (a . b)
parseFieldValue ((S.List [k, (S.Atom "."), v]):rest) = do
  kval <- parseSingleValue k
  vval <- parseSingleValue v
  return $ (PairValue kval vval, rest)
-- (...)
parseFieldValue ((S.List list):rest) = do
  parseFieldList False list rest
-- atom
parseFieldValue ((S.Atom atom):rest) = do
  val <- parseAtom (bs2text atom)
  return $ (val, rest)
-- []
parseFieldValue v = error $ "Unexpected value: " ++ (show v)

parseFieldList :: (Monad m, Applicative m) => Bool -> [S.Sexp] -> [S.Sexp] -> m (Value, [S.Sexp])
parseFieldList q list rest = do
  vals <- parseValuesList list
  let val = if (List.all isPair vals)
            then MapValue q . Map.fromList . fmap pairValueToPair $ vals
            else ListValue q vals
  return (val, rest)

parseSingleValue :: (Monad m, Applicative m) => S.Sexp -> m Value
parseSingleValue sexp = do
  (val, []) <- parseFieldValue [sexp]
  return val
                    
isPair :: Value -> Bool
isPair (PairValue _ _) = True
isPair _ = False

pairValueToPair :: Value -> (Value, Value)
pairValueToPair (PairValue k v) = (k, v)
pairValueToPair _ = error "Expected a pair."
    
parseAtom :: (Monad m, Applicative m) => Text -> m Value
parseAtom atom = return $ case (Text.uncons atom) of
  Just (':',_) -> LabelValue . Label $ atom
  Just (_,_) -> StringValue atom
  Nothing -> StringValue ""
    
parseValuesList :: (Monad m, Applicative m) => [S.Sexp] -> m [Value]
parseValuesList = mapM parseSingleValue
