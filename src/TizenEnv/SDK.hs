{-|
TizenEnv - An utility for setting up a Tizen development environment.
Copyright (C) 2016 Kamensky Timofey

This file is part of TizenEnv.

TizenEnv is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TizenEnv is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TizenEnv.  If not, see <http://www.gnu.org/licenses/>.
-}
{-# LANGUAGE OverloadedStrings, RecordWildCards #-}

module TizenEnv.SDK ( SDK(..)
                    , packageInfoPath
                    , sbiRootPath
                    , sbiPluginsPath
                    , initSDK
                    , configureTarget
                    , configureSBI
                    , loadPackageInfo
                    , loadDevPackage
                    , loadFirstDevPackage
                    ) where

import Prelude hiding (FilePath, pi)
import Data.Text (Text)
import Data.Map (Map)
import qualified Data.Map as Map
import Filesystem.Path.CurrentOS
import Control.Monad (liftM)

import TizenEnv.SBI
import TizenEnv.PackageInfo
import TizenEnv.PackageInfoXML
import TizenEnv.Utils
  
data SDK = SDK { sdkPath :: FilePath
               , sdkTargets :: Map Text Target
               , sdkRootstraps :: Map Text Rootstrap
               , sdkToolchains :: Map Text Toolchain
               }
         deriving (Show)

packageInfoPath :: FilePath -> FilePath
packageInfoPath r = r </> "platforms/tizen-2.4/mobile/rootstraps/info"

sbiRootPath :: FilePath -> FilePath
sbiRootPath r = r </> "tools/smart-build-interface"

sbiPluginsPath :: FilePath -> FilePath
sbiPluginsPath r = sbiRootPath r </> "plugins"

initSDK :: FilePath -> IO SDK
initSDK path = do
  let pluginsPath = sbiPluginsPath path
  pluginFiles <- listDirectory . encodeString $ pluginsPath
  plugins <- mapM (readPlugin . (pluginsPath </>) . decodeString) pluginFiles
  return $ foldr addPlugin (SDK path Map.empty Map.empty Map.empty) plugins
  where
    addPlugin plugin sdk@SDK{..} = case (plugExtension plugin) of
      ExtToolchain tc -> sdk { sdkToolchains = Map.insert (tcId tc) tc sdkToolchains }
      ExtRootstrap rs -> sdk { sdkRootstraps = Map.insert (rsId rs) rs sdkRootstraps }
      ExtTarget targ -> sdk { sdkTargets = Map.insert (targId targ) targ sdkTargets }

configureTarget :: SDK -> Text -> Maybe SBIConfig
configureTarget sdk@SDK{..} targetId = do
   Target{..} <- Map.lookup targetId sdkTargets
   rs <- Map.lookup targRootstrapId sdkRootstraps
   tc <- Map.lookup targToolchainId sdkToolchains
   return $ configureSBI sdk rs tc

configureSBI :: SDK -> Rootstrap -> Toolchain -> SBIConfig
configureSBI sdk rs tc = 
   let vars = Map.fromList [("SBI_HOME", forcePathToText . sbiRootPath . sdkPath $ sdk)
                           ]
   in SBIConfig vars rs tc

loadPackageInfo :: SBIConfig -> IO PackageInfo
loadPackageInfo sbi = do
  let Just path = Map.lookup "DEV_PACKAGE_CONFIG_PATH" . rsProperties . sbiRootstrap $ sbi
  readPackageInfo . fromText . expandVars sbi $ path
      
loadFirstDevPackage :: SBIConfig -> IO DevPackage
loadFirstDevPackage sbi = do
  pi <- loadPackageInfo sbi
  return . head . Map.elems . piDevPackages $ pi

loadDevPackage :: SBIConfig -> Text -> IO (Maybe DevPackage)
loadDevPackage sbi name =
  liftM (Map.lookup name . piDevPackages) $ loadPackageInfo sbi

