{-|
TizenEnv - An utility for setting up a Tizen development environment.
Copyright (C) 2016 Kamensky Timofey

This file is part of TizenEnv.

TizenEnv is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TizenEnv is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TizenEnv.  If not, see <http://www.gnu.org/licenses/>.
-}
{-# LANGUAGE OverloadedStrings #-}

module TizenEnv.PackageInfo ( PackageInfo(..)
                            , DevPackage(..)
                            ) where

import Data.Text (Text)
import Data.Map (Map)

data PackageInfo = PackageInfo { piDevPackages :: Map Text DevPackage
                               }
                 deriving (Eq, Show, Read)
  
data DevPackage = DevPackage { devpName :: Text
                             , devpIsFramework :: Bool
                             , devpIsVirtual :: Bool
                             , devpDescription :: Text
                             , devpIncludePaths :: [Text]
                             , devpLibraryPaths :: [Text]
                             , devpLibraries :: [Text]
                             }
                deriving (Eq, Show, Read)
