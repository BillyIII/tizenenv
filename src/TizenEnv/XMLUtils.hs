{-|
TizenEnv - An utility for setting up a Tizen development environment.
Copyright (C) 2016 Kamensky Timofey

This file is part of TizenEnv.

TizenEnv is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TizenEnv is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TizenEnv.  If not, see <http://www.gnu.org/licenses/>.
-}
{-# LANGUAGE OverloadedStrings, Arrows #-}

module TizenEnv.XMLUtils ( parseXML
                         , textArr
                         , textArr2
                         , getAttrText
                         , getAttrText0
                         , getAttrTextMaybe
                         , getChildrenText
                         , childrenList
                         , namedChildrenList
                         , childrenMap
                         , namedChildrenMap
                         , parseBool
                         , getAttrBool
                         ) where

import Prelude hiding (FilePath)
import qualified Data.Text as Text
import Data.Text (Text)
import qualified Data.Map as Map
import Data.Map (Map)
import Filesystem.Path.CurrentOS
import Text.XML.HXT.Core
  
parseXML :: FilePath -> String -> IOSArrow XmlTree a -> IO a
parseXML path name f = do
  -- print $ "parseXML " ++ (encodeString path)
  [r] <- runX $ readDocument [withValidate no
                             ] (encodeString path)
         >>>
         getChildren
         >>>
         (hasName name) `guards` f
  return r

textArr :: IOSArrow String Text
textArr = arr Text.pack

textArr2 :: IOSArrow (String, String) (Text, Text)
textArr2 = textArr *** textArr

getAttrText :: String -> IOSArrow XmlTree Text
getAttrText n = getAttrValue n >>^ Text.pack
           
getAttrText0 :: String -> IOSArrow XmlTree Text
getAttrText0 n = getAttrValue0 n >>^ Text.pack
           
getAttrTextMaybe :: String -> IOSArrow XmlTree (Maybe Text)
getAttrTextMaybe n = proc a -> do
  v <- getAttrValue n -< a
  case v of
    [] -> returnA -< Nothing
    s -> returnA -< Just (Text.pack s)

getChildrenText :: IOSArrow XmlTree Text
getChildrenText = listA (getChildren >>> isText `guards` getText)
                  >>^
                  (Text.pack . Prelude.concat)
           
childrenList :: IOSArrow XmlTree a -> IOSArrow XmlTree [a]
childrenList ch = listA (getChildren >>> ch)
                     
namedChildrenList :: String -> IOSArrow XmlTree a -> IOSArrow XmlTree [a]
namedChildrenList n ch = childrenList $ hasName n `guards` ch
                     
childrenMap :: (Ord k) => IOSArrow XmlTree (k,v) -> IOSArrow XmlTree (Map k v)
childrenMap ch = childrenList ch >>^ Map.fromList
                     
namedChildrenMap :: (Ord k) => String -> IOSArrow XmlTree (k,v) -> IOSArrow XmlTree (Map k v)
namedChildrenMap n ch = namedChildrenList n ch >>^ Map.fromList

parseBool :: String -> Bool
parseBool "true" = True
parseBool "false" = False
parseBool _ = error "Invalid boolean string."

getAttrBool :: String -> IOSArrow XmlTree Bool
getAttrBool n = getAttrValue0 n >>^ parseBool
