{-|
TizenEnv - An utility for setting up a Tizen development environment.
Copyright (C) 2016 Kamensky Timofey

This file is part of TizenEnv.

TizenEnv is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TizenEnv is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TizenEnv.  If not, see <http://www.gnu.org/licenses/>.
-}
{-# LANGUAGE OverloadedStrings, Arrows #-}

module TizenEnv.PackageInfoXML ( readPackageInfo
                               ) where

import Prelude hiding (FilePath)
import Data.Text (Text)
-- import qualified Data.Map as Map
-- import Data.Map (Map)
import Filesystem.Path.CurrentOS
import Text.XML.HXT.Core
  
import TizenEnv.XMLUtils
import TizenEnv.PackageInfo

readPackageInfo :: FilePath -> IO PackageInfo
readPackageInfo path = parseXML path "PackageInfo" parsePackageInfo
           
parsePackageInfo :: IOSArrow XmlTree PackageInfo
parsePackageInfo = (namedChildrenMap "DevPackage" parseDevPackage) >>^ PackageInfo

parseDevPackage :: IOSArrow XmlTree (Text, DevPackage)
parseDevPackage = proc t -> do
  name <- getAttrText0 "name" -< t
  isFramework <- getAttrBool "is_framework" -< t
  isVirtual <- getAttrBool "is_virtual" -< t
  descr <- (single $ getTextNodes "description") -< t
  includePaths <- (getTextNodesList "include_path") -< t
  libraryPaths <- (getTextNodesList "library_path") -< t
  libs <- (getTextNodesList "library") -< t
  returnA -< (name, DevPackage name isFramework isVirtual descr includePaths libraryPaths libs)
  
getTextNodes :: String -> IOSArrow XmlTree Text
getTextNodes name = getChildren >>> (hasName name `guards` getChildrenText)
                
getTextNodesList :: String -> IOSArrow XmlTree [Text]
getTextNodesList = listA . getTextNodes
