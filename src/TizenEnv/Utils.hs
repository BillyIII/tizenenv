{-|
TizenEnv - An utility for setting up a Tizen development environment.
Copyright (C) 2016 Kamensky Timofey

This file is part of TizenEnv.

TizenEnv is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TizenEnv is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TizenEnv.  If not, see <http://www.gnu.org/licenses/>.
-}
{-# LANGUAGE OverloadedStrings #-}

module TizenEnv.Utils ( listDirectory
                      , consMaybe
                      , consMaybeVar
                      , consVar
                      , forcePathToText
                      ) where

import Prelude hiding (FilePath)
import qualified Data.Text as Text
import Data.Text (Text)
import Filesystem.Path.CurrentOS
import System.Directory
import qualified GHC.IO

-- Some of the packages depend on the old directory package.
listDirectory :: GHC.IO.FilePath -> IO [GHC.IO.FilePath]
listDirectory path =
  (filter f) <$> (getDirectoryContents path)
  where f filename_ = filename_ /= "." && filename_ /= ".."
                     
consMaybe :: Maybe a -> [a] -> [a]
consMaybe (Just a) = (a:)
consMaybe Nothing = id
                           
consMaybeVar :: Text -> Maybe Text -> [(Text,Text)] -> [(Text,Text)]
consMaybeVar n v xs = consMaybe (fmap ((,) n) v) xs

consVar :: Text -> Text -> [(Text,Text)] -> [(Text,Text)]
consVar n v xs = (n,v):xs

forcePathToText :: FilePath -> Text
forcePathToText = either (error . ("Path cannot be converted to text: " ++) . Text.unpack) id . toText
