{-|
TizenEnv - An utility for setting up a Tizen development environment.
Copyright (C) 2016 Kamensky Timofey

This file is part of TizenEnv.

TizenEnv is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TizenEnv is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TizenEnv.  If not, see <http://www.gnu.org/licenses/>.
-}
{-# LANGUAGE OverloadedStrings, Arrows #-}

module TizenEnv.SBIPluginXML ( readPlugin
                             ) where

import Prelude hiding (FilePath)
import Data.Text (Text)
import Filesystem.Path.CurrentOS
import Text.XML.HXT.Core

import TizenEnv.XMLUtils
import TizenEnv.SBIPlugin

readPlugin :: FilePath -> IO Plugin
readPlugin path = parseXML path "extension" parseExtension
           
parseExtension :: IOSArrow XmlTree Plugin
parseExtension = (getAttrText0 "point" &&& (getChildren >>> parseExtensionNode))
                 >>^
                 uncurry Plugin

parseExtensionNode :: IOSArrow XmlTree Extension
parseExtensionNode = choiceA [ hasName "toolchain" :-> (parseToolchainNode >>^ ExtToolchain)
                             , hasName "rootstrap" :-> (parseRootstrapNode >>^ ExtRootstrap)
                             , hasName "target" :-> (parseTargetNode >>^ ExtTarget)
                             , this :-> none
                             ]

parseToolchainNode :: IOSArrow XmlTree Toolchain
parseToolchainNode = proc t -> do
  id_ <- getAttrText0 "id" -< t
  name <- getAttrText0 "name" -< t
  arch <- getAttrText0 "architecture" -< t
  path <- getAttrText0 "path" -< t
  toolchainType <- getAttrText0 "toolchainType" -< t
  props <- getProperties -< t
  tools <- getTools -< t
  actions <- getActions -< t
  returnA -< Toolchain id_ name arch path toolchainType props tools actions

parseRootstrapNode :: IOSArrow XmlTree Rootstrap
parseRootstrapNode = proc t -> do
  id_ <- getAttrText0 "id" -< t
  name <- getAttrText0 "name" -< t
  version <- getAttrText0 "version" -< t
  arch <- getAttrText0 "architecture" -< t
  path <- getAttrText0 "path" -< t
  toolchainType <- getAttrText0 "supportToolchainType" -< t
  props <- getProperties -< t
  returnA -< Rootstrap id_ name version arch path toolchainType props
                     
parseTargetNode :: IOSArrow XmlTree Target
parseTargetNode = proc t -> do
  id_ <- getAttrText0 "id" -< t
  name <- getAttrText0 "name" -< t
  rootstrap <- getAttrText0 "rootstrapId" -< t
  toolchain <- getAttrText0 "toolchainId" -< t
  returnA -< Target id_ name rootstrap toolchain
                     
-- getAttributes :: IOSArrow XmlTree TizenEnv.SBIPlugin.Attributes
-- getAttributes = (getAttrl >>> parseAttribute) >. arr Map.fromList
                
-- parseAttribute :: IOSArrow XmlTree (Text, Text)
-- parseAttribute = (getName >>^ Text.pack) &&& getChildrenText
                
getProperties :: IOSArrow XmlTree Properties
getProperties = namedChildrenMap "property" parseProperty
                
parseProperty :: IOSArrow XmlTree (Text, Text)
parseProperty = getAttrText0 "key" &&& getAttrText0 "value"

getTools :: IOSArrow XmlTree Tools
getTools = namedChildrenMap "tool" parseTool

parseTool :: IOSArrow XmlTree (Text, Tool)
parseTool = proc t -> do
  name <- getAttrText0 "name" -< t
  version <- getAttrTextMaybe "version" -< t
  path <- getAttrText0 "path" -< t
  returnA -< (name, Tool name version path)
            
getActions :: IOSArrow XmlTree Actions
getActions = namedChildrenMap "action" parseAction

parseAction :: IOSArrow XmlTree (Text, Action)
parseAction = (getAttrText0 "name" &&& getActionItems) >>^ \(n,is) -> (n, Action n is)

getActionItems :: IOSArrow XmlTree [ActionItem]
getActionItems = namedChildrenList "execute" parseExecuteNode

parseExecuteNode :: IOSArrow XmlTree ActionItem
parseExecuteNode = proc t -> do
  script <- getAttrText0 "script" -< t
  inputs <- getInputs -< t
  [body] <- getBodies -< t
  returnA -< Execute script inputs body

getInputs :: IOSArrow XmlTree [ExecInput]
getInputs = namedChildrenList "input" parseInput

parseInput :: IOSArrow XmlTree ExecInput
parseInput = (getAttrText0 "type" &&& getAttrText0 "value") >>^ uncurry ExecInput

getBodies :: IOSArrow XmlTree [Text]
getBodies = namedChildrenList "script" parseBody

parseBody :: IOSArrow XmlTree Text
parseBody = getChildrenText
