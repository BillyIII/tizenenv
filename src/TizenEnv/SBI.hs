{-|
TizenEnv - An utility for setting up a Tizen development environment.
Copyright (C) 2016 Kamensky Timofey

This file is part of TizenEnv.

TizenEnv is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TizenEnv is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TizenEnv.  If not, see <http://www.gnu.org/licenses/>.
-}
{-# LANGUAGE OverloadedStrings, RecordWildCards #-}

module TizenEnv.SBI ( module TizenEnv.SBIPlugin
                    , module TizenEnv.SBIPluginXML

                    , SBIConfig(..)
                    , expandVars
                    , expandTag
                    , sbiToolPath
                    , sbiRootstrapProp
                    , sbiToolchainProp
                    , sbiExpToolPath
                    , sbiExpRootstrapProp
                    , sbiExpToolchainProp
                    ) where

import Data.Text (Text)
import qualified Data.Text as Text
import Data.List (foldl')
import Data.Array ((!))
import Data.Map (Map)
import qualified Data.Map as Map
-- import Filesystem.Path.CurrentOS
import Text.Regex.TDFA

import TizenEnv.SBIPlugin
import TizenEnv.SBIPluginXML


data SBIConfig = SBIConfig { sbiVars :: Map Text Text
                           , sbiRootstrap :: Rootstrap
                           , sbiToolchain :: Toolchain
                           }
         deriving (Eq, Show, Read)
           
expandVars :: SBIConfig -> Text -> Text
expandVars SBIConfig{..} src = Text.pack $ expandString expr (Text.unpack src)
  where expr var = maybe "" Text.unpack $ Map.lookup (Text.pack var) sbiVars

-- regex-tdfa doesn't support Text.
expandString :: (String -> String) -> String -> String
expandString f s = start_ end
  where (_, end, start_) = foldl' go (0, s, id) m
        m = match re s :: [MatchArray]
        re = makeRegex ("(#{([^}]*)})" :: String) :: Regex
        go (ind,read_,write) arr =
          let (off,len) = arr ! 1
              (soff, slen) = arr ! 2
              (skip, start') = splitAt (off - ind) read_ 
              (_, remaining) = splitAt len start'
              smatched = take slen . snd . splitAt (soff - ind) $ read_ 
          in (off + len, remaining, write . (skip ++) . (f smatched ++))

-- TODO: xpath lookup?
expandTag :: SBIConfig -> Text -> Maybe Text
expandTag SBIConfig{..} s = let re = makeRegex ("#{([^}]*)}:([^:]*):([^:]*):([^:]*):([^:]*)" :: String) :: Regex
                                ms = match re (Text.unpack s) :: [MatchText String]
                            in case ms of
                            [] -> Nothing
                            (m:_) -> let mt n = fst (m ! n)
                                     in case (mt 1) of
                                     "TOOLCHAIN" -> expandToolchainTag sbiToolchain (mt 2) (mt 3) (mt 4) (mt 5)
                                     "ROOTSTRAP" -> expandRootstrapTag sbiRootstrap (mt 2) (mt 3) (mt 4) (mt 5)
                                     _ -> Nothing

expandToolchainTag :: Toolchain -> String -> String -> String -> String -> Maybe Text
expandToolchainTag tc "toolchain" "" "" resAttr =
  expandToolchainSelfTag tc resAttr
expandToolchainTag Toolchain{..} "property" testAttr testVal resAttr =
  expandPropertyTag tcProperties testAttr testVal resAttr
expandToolchainTag Toolchain{..} "tool" "name" testVal resAttr =
  Map.lookup (Text.pack testVal) tcTools >>= expandToolTag resAttr
expandToolchainTag _ _ _ _ _ = Nothing

expandToolchainSelfTag :: Toolchain -> String -> Maybe Text
expandToolchainSelfTag Toolchain{..} "id" = Just tcId
expandToolchainSelfTag Toolchain{..} "name" = Just tcName
expandToolchainSelfTag Toolchain{..} "architecture" = Just tcArchitecture
expandToolchainSelfTag Toolchain{..} "path" = Just tcPath
expandToolchainSelfTag Toolchain{..} "toolchainType" = Just tcToolchainType
expandToolchainSelfTag _ _ = Nothing

expandToolTag :: String -> Tool -> Maybe Text
expandToolTag "name" Tool{..} = Just toolName
expandToolTag "version" Tool{..} = toolVersion
expandToolTag "path" Tool{..} = Just toolPath
expandToolTag _ _ = Nothing

expandPropertyTag :: Properties -> String -> String -> String -> Maybe Text
expandPropertyTag _ "key" testVal "key" = Just (Text.pack testVal)
expandPropertyTag props "key" testVal "value" = Map.lookup (Text.pack testVal) props
expandPropertyTag _ _ _ _ = Nothing

expandRootstrapTag :: Rootstrap -> String -> String -> String -> String -> Maybe Text
expandRootstrapTag rs "rootstrap" "" "" resAttr =
  expandRootstrapSelfTag rs resAttr
expandRootstrapTag Rootstrap{..} "property" testAttr testVal resAttr =
  expandPropertyTag rsProperties testAttr testVal resAttr
expandRootstrapTag _ _ _ _ _ = Nothing

expandRootstrapSelfTag :: Rootstrap -> String -> Maybe Text
expandRootstrapSelfTag Rootstrap{..} "id" = Just rsId
expandRootstrapSelfTag Rootstrap{..} "name" = Just rsName
expandRootstrapSelfTag Rootstrap{..} "version" = Just rsVersion
expandRootstrapSelfTag Rootstrap{..} "architecture" = Just rsArchitecture
expandRootstrapSelfTag Rootstrap{..} "path" = Just rsPath
expandRootstrapSelfTag Rootstrap{..} "supportToolchainType" = Just rsSupportedToolchainType
expandRootstrapSelfTag _ _ = Nothing
                             
sbiToolPath :: SBIConfig -> Text -> Maybe Text
sbiToolPath sbi name = fmap toolPath . Map.lookup name . tcTools . sbiToolchain $ sbi

sbiRootstrapProp :: SBIConfig -> Text -> Maybe Text
sbiRootstrapProp sbi name = Map.lookup name . rsProperties . sbiRootstrap $ sbi

sbiToolchainProp :: SBIConfig -> Text -> Maybe Text
sbiToolchainProp sbi name = Map.lookup name . tcProperties . sbiToolchain $ sbi

expandLookup :: (SBIConfig -> Text -> Maybe Text) -> SBIConfig -> Text -> Maybe Text
expandLookup f sbi v = fmap (expandVars sbi) (f sbi v)

sbiExpToolPath :: SBIConfig -> Text -> Maybe Text
sbiExpToolPath = expandLookup sbiToolPath

sbiExpRootstrapProp :: SBIConfig -> Text -> Maybe Text
sbiExpRootstrapProp = expandLookup sbiRootstrapProp

sbiExpToolchainProp :: SBIConfig -> Text -> Maybe Text
sbiExpToolchainProp = expandLookup sbiToolchainProp
