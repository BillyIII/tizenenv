{-|
TizenEnv - An utility for setting up a Tizen development environment.
Copyright (C) 2016 Kamensky Timofey

This file is part of TizenEnv.

TizenEnv is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TizenEnv is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TizenEnv.  If not, see <http://www.gnu.org/licenses/>.
-}
{-# LANGUAGE OverloadedStrings, FlexibleContexts, RecordWildCards #-}

module Main ( main
            ) where

import Prelude hiding (FilePath, pi, print)
import Filesystem.Path.CurrentOS
import Data.Map (Map)
import qualified Data.Map as Map
import Data.Text (Text)
import qualified Data.Text as Text
import Data.Text.Encoding (encodeUtf8)
import qualified Data.ByteString.Lazy as BL
import Data.ByteString.Builder (Builder, byteString, toLazyByteString)
import Data.Monoid ((<>))
import Data.Text.Encoding (decodeUtf8)
import System.Environment (setEnv)
import Control.Monad (liftM)
import qualified Data.List as List
import Data.Text.Format
import Control.Monad (when)
import Options
import Data.Maybe (isJust)
       
import TizenEnv.SDK
import TizenEnv.SBI
import TizenEnv.PackageInfo
import TizenEnv.EnvVars
import TizenEnv.EmacsProject
import TizenEnv.Utils


data MainOptions = MainOptions { mainSDKPath :: String
                               , mainTarget :: Maybe String
                               , mainRootstrap :: Maybe String
                               , mainToolchain :: Maybe String
                               , mainDevPackage :: Maybe String
                               }
instance Options MainOptions where
    defineOptions = pure MainOptions
        <*> simpleOption "sdk" "" "Path to the tizen SDK."
        <*> simpleOption "target" Nothing "The name of the target to use."
        <*> simpleOption "rootstrap" Nothing "The name of the rootstrap to use. Overrides the target."
        <*> simpleOption "toolchain" Nothing "The name of the toolchain to use. Overrides the target."
        <*> simpleOption "devpackage" Nothing "The name of the dev package to use. The default is to use the first one in the file."

        
data EnvOpts = EnvOpts { envSaveFile :: Maybe String
                       , envSet :: Bool
                       , envPrint :: Bool
                       }
instance Options EnvOpts where
    defineOptions = pure EnvOpts
        <*> simpleOption "file" Nothing "Save the environment script to the specified file."
        <*> simpleOption "set" False "Modify the current environment."
        <*> simpleOption "print" False "Print the environment script."

        
data EmacsOpts = EmacsOpts { emacsProjectDir :: String
                           , emacsDryRun :: Bool
                           , emacsDestFile :: Maybe String
                           }
instance Options EmacsOpts where
    defineOptions = pure EmacsOpts
        <*> simpleOption "project" "" "Project directory."
        <*> simpleOption "dry" False "Print the result and exit without modifying the project."
        <*> simpleOption "dest" Nothing "Save result to a different file."

        
data InfoOpts = InfoOpts { infoAll :: Bool
                         , infoTargets :: Bool
                         , infoRootstraps :: Bool
                         , infoToolchains :: Bool
                         , infoDevPackages :: Bool
                         }
instance Options InfoOpts where
    defineOptions = pure InfoOpts
        <*> simpleOption "all" False "Display all available information."
        <*> simpleOption "targets" False "Display information about available targets."
        <*> simpleOption "rootstraps" False "Display information about available rootstraps."
        <*> simpleOption "toolchains" False "Display information about available toolchains."
        <*> simpleOption "dev-packages" False "Display information about available dev packages (requires a valid target)."

        
env :: MainOptions -> EnvOpts -> [String] -> IO ()
env mainOpts EnvOpts{..} [] = do
  (sbi, devp) <- loadTarget mainOpts
  let vars = createBuildEnv sbi devp
  when envSet $ applyEnv vars
  when envPrint $ printEnv vars
  maybe (return ()) (saveEnv vars) envSaveFile
env _ _ args = error ("Unexpected arguments at the end: " ++ show args)

applyEnv :: Map Text Text -> IO ()
applyEnv = mapM_ setkv . Map.assocs
  where setkv (k,v) = setEnv (Text.unpack k) (Text.unpack v)

printEnv :: Map Text Text -> IO ()
printEnv = BL.putStr . toLazyByteString . buildEnvString

saveEnv :: Map Text Text -> String -> IO ()
saveEnv vars path = BL.writeFile path . toLazyByteString . buildEnvString $ vars
                        
buildEnvString :: Map Text Text -> Builder
buildEnvString = foldr addline mempty . Map.assocs
  where addline (k,v) b = b <> "export " <> (bstr k) <> "=" <> (bstr v) <> "\n"
        bstr = byteString . encodeUtf8


emacs :: MainOptions -> EmacsOpts -> [String] -> IO ()
emacs mainOpts EmacsOpts{..} [] = do
  (sbi, devp) <- loadTarget mainOpts
  let path = (decodeString emacsProjectDir) </> "project.el"
  proj <- readProject path
  let proj' = reconfigureProject sbi devp proj
  if emacsDryRun
    then printProject proj'
    else do let destFile = maybe path decodeString emacsDestFile
            writeProject destFile proj'
emacs _ _ args = error ("Unexpected arguments at the end: " ++ show args)

printProject :: Project -> IO ()
printProject = putStrLn . Text.unpack . decodeUtf8 . BL.toStrict . makeProjectString


info :: MainOptions -> InfoOpts -> [String] -> IO ()
info mainOpts InfoOpts{..} [] = do
  sdk <- loadTargetSDK mainOpts

  sbi <- if hasTarget mainOpts
         then liftM Just $ loadTargetSBI mainOpts sdk
         else return Nothing
    
  printGeneralInfo sdk sbi
  when (infoAll || infoTargets) $ printTargetsInfo sdk sbi
  when (infoAll || infoRootstraps) $ printRootstrapsInfo sdk sbi
  when (infoAll || infoToolchains) $ printToolchainsInfo sdk sbi
  when (infoAll || infoDevPackages) $ printDevPackagesInfo sdk sbi
info _ _ args = error ("Unexpected arguments at the end: " ++ show args)

hasTarget :: MainOptions -> Bool
hasTarget MainOptions{..} =
  isJust mainTarget
  || isJust mainRootstrap
  || isJust mainToolchain
  || isJust mainDevPackage
  
tryExpandText :: Maybe SBIConfig -> Text -> Text
tryExpandText sbi t = maybe t (flip expandVars t) sbi

tryExpandPath :: Maybe SBIConfig -> FilePath -> Text
tryExpandPath sbi p = tryExpandText sbi $ forcePathToText p
  
printGeneralInfo :: SDK -> Maybe SBIConfig -> IO ()
printGeneralInfo sdk sbi = do
  putStrLn "General info"
  print "  SDK path: {}\n" $ Only (tryExpandPath sbi $ sdkPath sdk)
  print "  Targets: {}\n" $ Only (Map.size $ sdkTargets sdk)
  print "  Rootstraps: {}\n" $ Only (Map.size $ sdkRootstraps sdk)
  print "  Toolchains: {}\n" $ Only (Map.size $ sdkToolchains sdk)

printTargetsInfo :: SDK -> Maybe SBIConfig -> IO ()
printTargetsInfo sdk sbi = mapM_ (printTargetInfo sbi) . Map.elems . sdkTargets $ sdk

printTargetInfo :: Maybe SBIConfig -> Target -> IO ()
printTargetInfo _ Target{..} = do
  putStr "\nTarget\n"
  print "  Id: {}\n" $ Only targId
  print "  Name: {}\n" $ Only targName
  print "  Rootstrap: {}\n" $ Only targRootstrapId
  print "  Toolchain: {}\n" $ Only targToolchainId
    
printRootstrapsInfo :: SDK -> Maybe SBIConfig -> IO ()
printRootstrapsInfo sdk sbi = mapM_ (printRootstrapInfo sbi) . Map.elems . sdkRootstraps $ sdk

printRootstrapInfo :: Maybe SBIConfig -> Rootstrap -> IO ()
printRootstrapInfo sbi Rootstrap{..} = do
  putStr "\nRootstrap\n"
  print "  Id: {}\n" $ Only rsId
  print "  Name: {}\n" $ Only rsName
  print "  Version: {}\n" $ Only rsVersion
  print "  Architecture: {}\n" $ Only rsArchitecture
  print "  Path: {}\n" $ Only $ tryExpandText sbi rsPath
  print "  rsSupportedToolchainType: {}\n" $ Only rsSupportedToolchainType
    
printToolchainsInfo :: SDK -> Maybe SBIConfig -> IO ()
printToolchainsInfo sdk sbi = mapM_ (printToolchainInfo sbi) . Map.elems . sdkToolchains $ sdk

printToolchainInfo :: Maybe SBIConfig -> Toolchain -> IO ()
printToolchainInfo sbi Toolchain{..} = do
  putStr "\nToolchain\n"
  print "  Id: {}\n" $ Only tcId
  print "  Name: {}\n" $ Only tcName
  print "  Architecture: {}\n" $ Only tcArchitecture
  print "  Path: {}\n" $ Only $ tryExpandText sbi tcPath
  print "  Type: {}\n" $ Only tcToolchainType

printDevPackagesInfo :: SDK -> Maybe SBIConfig -> IO ()
printDevPackagesInfo _ maybeSbi = do
  case maybeSbi of
    Nothing -> putStr "\nTarget specification is required to display information about the dev packages.\n"
    Just sbi -> do
      pi <- loadPackageInfo sbi
      mapM_ printDevPackageInfo $ piDevPackages pi

printDevPackageInfo :: DevPackage -> IO ()
printDevPackageInfo DevPackage{..} = do
  putStr "\nDev package\n"
  print "  Name: {}\n" $ Only devpName
  print "  Is framework: {}\n" $ Only devpIsFramework
  print "  Is virtual: {}\n" $ Only devpIsVirtual
  print "  Description: {}\n" $ Only devpDescription

    
loadTargetSDK :: MainOptions -> IO SDK
loadTargetSDK = initSDK . decodeString . mainSDKPath
    
loadTargetSBI :: MainOptions -> SDK -> IO SBIConfig
loadTargetSBI MainOptions{..} sdk = do
  let target = case mainTarget of
        Nothing -> error "Incomplete target specification."
        Just targetId -> maybe (error "Target not found.") id $ Map.lookup (Text.pack targetId) (sdkTargets sdk)
    
  let rsName = maybe (targRootstrapId target) Text.pack mainRootstrap
  let tcName = maybe (targToolchainId target) Text.pack mainToolchain
    
  let rs = maybe (error "Rootstrap not found.") id $ Map.lookup rsName (sdkRootstraps sdk)
  let tc = maybe (error "Toolchain not found.") id $ Map.lookup tcName (sdkToolchains sdk)
    
  return $ configureSBI sdk rs tc

loadTargetDevPackage :: MainOptions -> SBIConfig -> IO DevPackage
loadTargetDevPackage MainOptions{..} sbi = do
  devps <- liftM piDevPackages $ loadPackageInfo sbi
    
  let maybeDevp = case mainDevPackage of
        Nothing -> fmap fst . List.uncons . Map.elems $ devps
        Just devpName -> Map.lookup (Text.pack devpName) devps

  return $ maybe (error "Dev package not found.") id maybeDevp
    
loadTarget :: MainOptions -> IO (SBIConfig, DevPackage)
loadTarget mainOpts = do
  sdk <- loadTargetSDK mainOpts
  sbi <- loadTargetSBI mainOpts sdk
  devp <- loadTargetDevPackage mainOpts sbi
  return (sbi, devp)

  
main :: IO ()
main = runSubcommand [ subcommand "env" env
                     , subcommand "emacs" emacs
                     , subcommand "info" info
                     ]
